from django.urls import path

from . import views

urlpatterns = [
	# clear all cache section
	path('refresh', views.refresh),

	# Estimated services section
	path('notice/reminder', views.UserNotice.reminder),
	path('transaction', views.Transactions.browse),
	path('detail/transaction', views.Transactions.detail_transaction),
	path('estimated/vehicle', views.EstimatedServices.getVehicle),
	path('estimated/getoilprice', views.EstimatedServices.getOilPrice),
	path('estimated/getTirePrice', views.EstimatedServices.getTirePrice),
	path('estimated/getAccuPrice', views.EstimatedServices.getAccuPrice),

	# Registrations scion
	path('get/telp', views.UserLogin.getPersonNumber),

	# Product & Service
	path('get/product-type', views.Products.type),
	path('get/product-brand-by-type', views.Products.brand),
	path('get/product-by-brand-type', views.Products.list),
	path('get/product-search', views.Products.search),
	path('get/product-reminder', views.Products.reminder),
	path('get/detail/reminder', views.Products.detail_reminder),

	path('get/count/reminder', views.Products.count_reminder),

	path('get/brand-id', views.Products.brand_by_id),

]